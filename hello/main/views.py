from . import main_blueprint

@main_blueprint.route("/")
def main_index():
    return "Hello World!"
