from flask import Flask

def create_app():
    app = Flask(__name__)

    # app.config.from_pyfile(
    #     os.path.join(
    #         app.instance_path,
    #         os.environ.get("FLASK_CONFIG", "development.cfg")))

    from hello.main import main_blueprint
    app.register_blueprint(main_blueprint)

    return app
